## About Assignment

It is made in Laravel 9.X and using Scribe for Documentation

### Running Steps

- Create the database 
- Copy the .env file
- Modify the database name in .env
- ```php artisan migrate```
- ```php artisan db:seed```
- ```php artisan serve```
- For testing ```php artisan test```
- To visit the documentation Go to ```BASE_URL{actual URL of the application}/docs```
