<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->id();
            $table->string('ingredient_name');
            $table->double('in_stock_value');
            $table->double('out_stock_value');
            $table->float('ingredient_price');
            $table->enum('measuring_unit', ['kg', 'l'])->default('kg');
            $table->dateTime('merchant_notified_at')->nullable();
            $table->enum('status', ['active', 'delete'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
};
