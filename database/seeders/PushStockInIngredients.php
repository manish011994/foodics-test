<?php

namespace Database\Seeders;

use App\Models\ProductIngredient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Ingredient;

class PushStockInIngredients extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ingredient::create([
            'ingredient_name' => 'Beef',
            'in_stock_value' => 20,
            'out_stock_value' => 0,
            'ingredient_price' => 180
        ]);
        Ingredient::create([
            'ingredient_name' => 'Cheese',
            'in_stock_value' => 5,
            'out_stock_value' => 0,
            'ingredient_price' => 500
        ]);
        Ingredient::create([
            'ingredient_name' => 'Onion',
            'in_stock_value' => 1,
            'out_stock_value' => 0,
            'ingredient_price' => 20
        ]);

        $products = Product::factory(10)->create();

        if (is_iterable($products) && count($products)) {
            foreach ($products as $product) {
                ProductIngredient::create([
                    'product_id' => $product->id,
                    'ingredient_id' => Ingredient::where('ingredient_name', 'Beef')->first()->id,
                    'ingredient_quantity' => rand(150, 250) / 1000,
                ]);
                ProductIngredient::create([
                    'product_id' => $product->id,
                    'ingredient_id' => Ingredient::where('ingredient_name', 'Cheese')->first()->id,
                    'ingredient_quantity' => rand(30, 50) / 1000,
                ]);
                ProductIngredient::create([
                    'product_id' => $product->id,
                    'ingredient_id' => Ingredient::where('ingredient_name', 'Onion')->first()->id,
                    'ingredient_quantity' => rand(20, 50) / 1000,
                ]);
            }
        }
    }
}
