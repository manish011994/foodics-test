<p>Dear Merchant,</p>

<p>The inventory stock of {{ $ingredient->ingredient_name }} reached below 50%. Please add the stock in inventory.</p>

<p>Thank You</p>
