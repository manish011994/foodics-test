<?php

namespace App\Http\Responses;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;

class NewOrderResponse implements Responsable
{
    private Order $order;

    public function __construct($orderData)
    {
        $this->order = $orderData;
    }

    public function toResponse($request): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => [
                'message' => 'Order created successfully',
                'order' => $this->order
            ],
            'meta' => [
                'timestamp' => Carbon::now()
            ],
        ], 201);
    }
}
