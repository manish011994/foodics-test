<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewOrderRequest;
use App\Services\OrderService;
use Illuminate\Http\Request;

class NewOrder extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(NewOrderRequest $request)
    {
        $service = new OrderService();
        return $service->handleOrder($request->validated());
    }
}
