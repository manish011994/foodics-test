<?php

namespace App\Listeners;

use App\Events\NotifyMerchant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\IngredientLevelBelow;

class EmailMerchantForIngredientLevel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NotifyMerchant  $event
     * @return void
     */
    public function handle(NotifyMerchant $event)
    {
        dd("coming");
        // Passed Hardcoded email
        Mail::to('merchant@example.com')->send(new IngredientLevelBelow($event->ingredient));
    }
}
