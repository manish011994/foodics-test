<?php

namespace App\Listeners;

use App\Events\NewOrderCreated;
use App\Events\NotifyMerchant;
use App\Models\Ingredient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckForIngredientLevel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\NewOrderCreated $event
     * @return void
     */
    public function handle(NewOrderCreated $event)
    {
        $ingredientIds = $event->order->getUniqueOrderIngredientIds();
        foreach ($ingredientIds as $ingredientId) {
            $ingredient = Ingredient::find($ingredientId);
            //Check If Ingredient Reached Below 50% Level and If Merchant Recently Notified
            if ($ingredient->isIngredientReachedBelowLevel() && !$ingredient->isMerchantRecentlyNotified()) {
                $ingredient->updateMerchantNotificationDateTime();
                dd($ingredient);
                event(new NotifyMerchant($ingredient));
            }
        }
    }
}
