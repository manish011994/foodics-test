<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ingredient extends Model
{
    use HasFactory;

    public function isIngredientReachedBelowLevel(): bool
    {
        return ($this->out_stock_value / $this->in_stock_value) * 100 >= 50;
    }

    public function isMerchantRecentlyNotified(): bool
    {
        return $this->merchant_notified_at != null;
    }

    public function updateMerchantNotificationDateTime(): Ingredient
    {
        $this->merchant_notified_at = date('Y-m-d H:i:s');
        $this->save();
        return $this;
    }

}
