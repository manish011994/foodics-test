<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderDetails extends Model
{
    use HasFactory;

    protected $fillable = ['order_id', 'product_id', 'quantity'];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function checkIfEnoughIngredientToFulFillOrder() : bool
    {
        if ($this->product) {
            foreach ($this->product->productIngredients as $productIngredient) {
                $ingredient = $productIngredient->ingredient;
                $ingredient->out_stock_value += $productIngredient->ingredient_quantity * $this->quantity;
                if($ingredient->out_stock_value > $ingredient->in_stock_value) {
                    return false;
                }
            }
        }
        return true;
    }

    public function updateOutStockValue(): void
    {
        if ($this->product) {
            foreach ($this->product->productIngredients as $productIngredient) {
                $ingredient = $productIngredient->ingredient;
                $ingredient->out_stock_value += $productIngredient->ingredient_quantity * $this->quantity;
                $ingredient->save();
            }
        }
    }

    public function getOrderIngredients(): array
    {
        $ingredients = [];
        foreach ($this->product->productIngredients as $productIngredient) {
            if(!in_array($productIngredient->ingredient->id, $ingredients)) {
                $ingredients[] = $productIngredient->ingredient->id;
            }
        }
        return $ingredients;
    }
}
