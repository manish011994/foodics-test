<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['ordered_by', 'order_total'];

    /**
     * @return HasMany
     */
    public function orderDetails(): HasMany
    {
        return $this->hasMany(OrderDetails::class);
    }

    public function getUniqueOrderIngredientIds(): array
    {
        $ingredients = [];
        foreach ($this->orderDetails as $orderDetail) {
            $ingredients[] = $orderDetail->getOrderIngredients();
        }
        $ingredients = call_user_func_array('array_merge', $ingredients);
        return array_unique($ingredients);
    }

}
