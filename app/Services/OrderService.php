<?php

namespace App\Services;

use App\Events\NewOrderCreated;
use App\Http\Responses\NewOrderResponse;
use App\Models\Order;
use App\Models\OrderDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

class OrderService
{
    /**
     * @var Order
     */
    protected Order $order;
    protected array $orderData;

    public function handleOrder(array $orderRequest): mixed
    {
        $this->assignOrderData($orderRequest);
        $this->createOrder();
        $processOrder = $this->canOrderBeFulfilled();
        if ($processOrder) {
            $this->createOrderDetails();
            $this->triggerEvent();
        } else {
            return response()->json([
                'success' => false,
                'data' => [
                    'message' => 'Order cannot be fulfilled as some ingredients are not adequate.'
                ],
                'meta' => [
                    'timestamp' => Carbon::now()
                ],
            ], 400);
        }
        return new NewOrderResponse($this->order);
    }

    private function assignOrderData(array $orderRequest): void
    {
        $this->orderData = $orderRequest;
    }

    private function createOrder(): Order
    {
        return $this->order = Order::create([
            'ordered_by' => 1, // Passing hardcoded User Id
            'order_total' => rand(50, 500) * rand(1, 10),
        ]);
    }

    private function canOrderBeFulfilled(): bool
    {
        foreach ($this->orderData['products'] as $product) {
            $orderDetails = new OrderDetails();
            $orderDetails->order_id = $this->order->id;
            $orderDetails->product_id = $product['product_id'];
            $orderDetails->quantity = $product['quantity'];
            $canOrderBeProcessed = $orderDetails->checkIfEnoughIngredientToFulFillOrder();
            if(!$canOrderBeProcessed) {
                return false;
            }
        }
        return true;
    }

    private function createOrderDetails(): OrderService
    {
        foreach ($this->orderData['products'] as $product) {
            $orderDetails = OrderDetails::create([
                'order_id' => $this->order->id,
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity'],
            ]);
            $orderDetails->updateOutStockValue();
        }
        return $this;
    }

    private function triggerEvent(): void
    {
        event(new NewOrderCreated($this->order));
    }
}
